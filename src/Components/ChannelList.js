import React, { Component } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import gql from "graphql-tag";
import { Query } from "react-apollo";

const GET_CHANNELS = gql`
  {
    channels {
      id
      name
    }
  }
`;


class ChannelList extends Component {
    styles = theme => ({
        root: {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
        },
    });

    render() {

        return(
            <div className={this.styles.root}>
            <Query query={GET_CHANNELS}>
                {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return `Error! ${error.message}`;
                return (
                    <List component="nav">
                    {data.channels.map(channel => (
                        <ListItem button key={channel.id} onClick={e => this.props.setChannelIdCallback(channel.id, channel.name)}>
                            <ListItemText primary={channel.name} />
                        </ListItem>
                    ))}
                    </List>
                );}}
            </Query>
            </div>
        )
    }
}
export default ChannelList;
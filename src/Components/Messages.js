import React, {Component} from 'react'
import ListItem from '@material-ui/core/ListItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'
import { ListItemText } from '@material-ui/core';

class Messages extends Component 
{
  styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
  });

  formatMsgDateTime (createdOn) {
    var date = Date.parse(createdOn.replace("[GMT]", ""));
    return (new Intl.DateTimeFormat('en-US', {
      weekday: 'short',
      year: '2-digit',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    }).format(date))
  }

  render() 
  {
    let {loading, error, messages} = this.props.data
    const classes = this.styles
    return (
    	<div>
    		{loading && <ListItem> <CircularProgress /> </ListItem>}
    		{error && <ListItem><ListItemText primary='Error' secondary={error.message} /></ListItem>}
        {messages && messages.content
        .filter(({channel}) => {
          return channel.id === this.props.selectedChannelId
        })
        .reverse()
        .map( ({message, id, user, createdOn}) => 
        <ListItem alignItems="flex-start" key={id}>
        <ListItemAvatar>
          <Avatar className={classes.avatar}>{user.username.charAt(0).toUpperCase()}</Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={message}
          secondary={
            <React.Fragment>
              <Typography component="span" className={classes.inline} color="textPrimary">
                {user.username}
              </Typography>
              {this.formatMsgDateTime(createdOn)}
            </React.Fragment>
          }
        />
      </ListItem>)}
    	</div>
    	)
    
  }
}

export const messagesListQuery = gql`
  query MessagesQuery {
    messages {
      content {
        id
        message
        createdOn
        user {
          username
        }
        channel {
          id
        }
      }
    }
  }
 `

const ApolloMessages = graphql(messagesListQuery, {options: { pollInterval: 10000, fetchPolicy: 'cache-and-network' }})(Messages)

export default ApolloMessages
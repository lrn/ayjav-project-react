import React, {Component} from 'react'

import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import {graphql} from 'react-apollo'
import gql from 'graphql-tag'
import { messagesListQuery } from './Messages'


class MessageSender extends Component {
  state = {
    textValue: ''
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() 
  {
    var {mutate, userId, username, channelId} = this.props
  	var {textValue} = this.state
    return(
    	<ListItem disabled = {true} >
			<TextField disabled = {!userId} style = {{width: '90%'}} placeholder={userId ? "Type Message as "+ username : "Type Message"} value = {textValue} onChange = {this.handleChange('textValue')} />
			<Button mini = {true} onClick = { _ => {
				if(textValue)
				{
					mutate({
            variables: {message: textValue, userId: userId, channelId: channelId},
            refetchQueries: [{
              query: messagesListQuery
            }]
          })
					this.setState({textValue: ''})
				}
			}}>
		    	<i className="material-icons">send</i>
		    </Button>
		</ListItem>
      )
  }
}

const messageSenderMutation = gql`
mutation messageSenderMutation($message: String!, $userId: Long!, $channelId: Long!) 
{
  addMessage(message: {
    message: $message,
    userId: $userId,
    channelId: $channelId,
  })
  {
    id
    message
    createdOn
    user {
      username
    }
    channel {
      id
    }
  }
}
`

const ApolloMessageSender = graphql(messageSenderMutation)(MessageSender)

export default ApolloMessageSender
import React, { Component } from 'react'
import {  Mutation, ApolloConsumer } from 'react-apollo'
import gql from 'graphql-tag';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

class Login extends Component {

  state = {
    login: true, // switch between Login and SignUp
    fullname: '',
    username: '',
    errorMsg: '',
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { login, fullname, username, errorMsg } = this.state
    const { classes } = this.props;
    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" open={this.props.open}>
        <main className={classes.main}></main>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form}>

            <FormControl margin="normal" fullWidth error={errorMsg}>
              <InputLabel htmlFor="username">Username</InputLabel>
              <Input id="username" name="username" autoFocus value={username} 
                onChange={this.handleChange('username')}/>
            </FormControl>

            {!login && (
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="name">Full Name</InputLabel>
                <Input name="fullname" type="text" id="fullname" value={fullname} 
                  onChange={this.handleChange('fullname')}/>
              </FormControl>
            )}
            {errorMsg && (
              <FormHelperText error={errorMsg}> {errorMsg} </FormHelperText>)}
            {!login && (
              <Mutation
                // mutation={login ? LOGIN_MUTATION : SIGNUP_MUTATION}
                mutation={SIGNUP_MUTATION}
                variables={{username, fullname }}
                onCompleted={data => this._confirm(data, 'mutation')}
              >
                {mutation => (
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={mutation}
                  >
                  create account
                  </Button>
                )}
              </Mutation>
            )}
            {login && (
              <ApolloConsumer>
                {client => (
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={async () => {
                      const { data } = await client.query({
                        query: GET_USERID_BY_NAME,
                        variables: { username: username }
                      });
                      this._confirm(data, 'query');
                    }}
                  >
                    Log in
                  </Button>
                )}
              </ApolloConsumer>
            )}
            <Button
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
              onClick={() => this.setState({ login: !login })}
            >
              {login
              ? 'need to create an account?'
              : 'already have an account?'}
            </Button>
          </form>
        </Paper>
      </Dialog>
    )
  }

  _confirm = async (data, type) => {
    console.log(data, type)
    if (type === 'mutation') {
      this._saveUserData(data.addUser.id, data.addUser.username)
    } else {
      if (data.users.length < 1) {
        this.setState({errorMsg: "User doesn't exists"})
      }
      else {
        this._saveUserData(data.users[0].id, data.users[0].username)
      }
    }
  }

  _saveUserData = (userId, username) => {
    console.log(userId, username)
    this.props.setUserIdCallback(userId, username)
  }
}

const SIGNUP_MUTATION = gql`
  mutation AddUser($fullname: String!, $username: String!) {
    addUser(user: {fullname: $fullname, username: $username}) {
      id,
      username
    }
  }
`

const GET_USERID_BY_NAME  = gql`
  query UserId($username: String!){
    users(filter: {username: $username})
  {
	  id,
    username
  }}
`
Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

const LoginWrapped = withStyles(styles)(Login);
export default LoginWrapped
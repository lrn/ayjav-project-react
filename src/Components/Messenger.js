import React, {Component} from 'react'

import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

import ApolloMessages from './Messages'
import ApolloMessageSender from './MessageSender'
import Login from './Login'
import ChannelList from './ChannelList'


class Messenger extends Component {
	state = {
		userId: localStorage.getItem("userId"),
		username: localStorage.getItem("username"),
	}

	styles = theme => ({
		root: {
			flexGrow: 1,
		},
		paper: {
			padding: theme.spacing.unit * 2,
			textAlign: 'center',
			color: theme.palette.text.secondary,
		},
	});

	componentDidMount() {
		this.setState({channelId: this.props.initChannelId, 
			channelName: this.props.initChannelName})
  }

	setChannelId = (id, name) => {
		console.log('channelId', id)
		this.setState({channelId: id, channelName: name})
	}

	handleUsernameChange = (e) => {
		this.setState({username: e.target.value})
	}

	setUserId = (userId, username) => {
		this.setState({username: username, userId: userId})
		localStorage.setItem('username', username)
    localStorage.setItem('userId', userId)
	}

	handleLogOut = (e) => {
		this.setState({username: '', userId: ''})
		localStorage.setItem('username', '')
    localStorage.setItem('userId', '')
	}
	
	render() {
		const classes = this.styles
    return(
			<div className={classes.root}>
      <Grid container spacing={16}>
				<Grid item xs={12}>
					<Paper elevation={1} style = {{padding: "1em"}} >
					{this.state.userId && (
						<Typography inline={true} variant="h6" component="h3">
							Logged in as {this.state.username}
						</Typography>
					)}
						<Button variant="outlined" color="primary" onClick={this.handleLogOut} style = {{marginLeft: "30px"}}>
									Log out
						</Button>			
					</Paper>
				<Login open={!this.state.userId} setUserIdCallback = {this.setUserId} />
        </Grid>
				<Grid item xs={2} >
				<Paper style = {{padding: "1em"}} >
					<Typography variant="h5" component="h3">
							Channels
					</Typography>
					<Divider />
					<ChannelList setChannelIdCallback={this.setChannelId}/>
				</Paper>
				</Grid>
				<Grid item xs={6}>
					<Card>
							<CardContent>
								<Typography align="right" variant="subtitle1" component="h3">
									Channel: <b>{this.state.channelName}</b>
								</Typography>
								<Divider />
								<List>
									<div style = {{maxHeight: 300, minHeight: 300, overflow: 'scroll'}}>
										<ApolloMessages selectedChannelId={this.state.channelId} />
									</div>
									<Divider/>
									{ this.state.userId ? <ApolloMessageSender userId = {this.state.userId} username = {this.state.username} 
									channelId= {this.state.channelId} /> : null }
								</List>
							</CardContent>
						</Card>
				</Grid>
			</Grid>
			</div>
		)
  }
}

export default Messenger
	
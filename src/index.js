import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'
// main app
import ApolloMessenger from './Components/Messenger'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import blue from '@material-ui/core/colors/blue';

import {ApolloProvider} from 'react-apollo'

import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-client-preset'


const theme = createMuiTheme({
	palette: {
	  primary: blue,
	},
	typography: {
    useNextVariants: true,
  },
  });

const client = new ApolloClient({
	link: new HttpLink({uri: 'http://localhost:8088/graphql'}),
  	cache: new InMemoryCache(),
})

ReactDOM.render(	
	<ApolloProvider client={client} >
		<MuiThemeProvider theme={theme}>
      		<ApolloMessenger client={client} initChannelId="4" initChannelName="general"/>
		</MuiThemeProvider>
	</ApolloProvider>,
	document.getElementById('root')
)
serviceWorker.unregister()
